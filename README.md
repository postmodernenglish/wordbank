# WordBank
Contains audio files of words from different accents across the world.

In order to commit to the WordBank, please fork the Master branch, make changes in the fork, and then open a pull request back to the Master branch.
If you want to commit to the WordBank, please find the folder containing words in the accent that is closest to yours. When committing, please ensure that the following criteria are met:

- Please make sure that you are putting files in the correct accent and letter folders for each word you upload
- Postmodern audio files must use arguably correct Postmodern English spelling, based on the accent heard. Spelling in Postmodern English is based on how a word sounds when spoken in isolation from any other words. Recordings with additional content beyond the word will not be accepted.
- Make sure that the file name is in the format: (Standard English Spelling) Postmodern spelling of the word in your accent-year recorded.mp3. All words must have the first letter capitalized in this format.
- Audio files must be 200 KB or smaller in size, or else they will not be accepted
- Please only commit MP3 audio files to the WordBank. Any other file type will not be accepted.
- When exporting the MP3, please try to normalize the volume of the audio to -1.0 dB. Excessively loud or quiet recordings will not be accepted.
- Please ensure that there is not more than 3 seconds of silence before or after the spoken word
- When recording words, please try to reduce background noise as much as possible, so that the word audio can be as clear as possible. Excessively noisy or low quality recordings will not be accepted.
- Please record words in a normal speaking voice and tone
- Recorded words must be of the user's own making, and not taken from somewhere else
- Please don't upload any words or terms which may be subject to copyright
- If a Standard English word has multiple acceptable pronunciations for the same spelling, audio for both pronunciations may be uploaded. For example: Close could mean Kloz (as in closing a door), or Klos (as in getting close to something).
- Do not replace any audio files that already exist if they are marked with the current year. For example, if the word "hello" is already in the WordBank for 2021 in the Midland accent, do not replace or upload a duplicate of that word. However, by 2022, the Postmodern Spelling may be contested if you feel it has changed.
- If you feel that an important accent has not been listed in this repository, please reach out to the Postmodern English team. Do not add accent folders on your own.
- Please only upload 50 or fewer audio files per forked branch. This makes changes easier to review. If you would like to upload more than 50 new files, please create separate forks.

A popular and free program that can be used to record and save MP3 files is called Audacity. Please consider committing a word or two for your accent, as every little bit helps. We hope you will find the WordBank useful.